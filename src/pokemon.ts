export class Pokemon {
    public name : string ;
    public speed : number;
    public lifePoint : number;
    public damagePoint : number;

    constructor(nameParam : string, speedParam : number, lifePointParam: number, damagePointParam : number){
        this.name = nameParam;
        this.speed = speedParam;
        this.lifePoint = lifePointParam;
        this.damagePoint = damagePointParam;
    }

    public receiveDamage(damage : number){
        this.lifePoint -= damage;
        if(this.lifePoint < 0){
            this.lifePoint = 0;
        }
    }
}