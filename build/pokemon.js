"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Pokemon {
    constructor(nameParam, speedParam, lifePointParam, damagePointParam) {
        this.name = nameParam;
        this.speed = speedParam;
        this.lifePoint = lifePointParam;
        this.damagePoint = damagePointParam;
    }
    receiveDamage(damage) {
        this.lifePoint -= damage;
        if (this.lifePoint < 0) {
            this.lifePoint = 0;
        }
    }
}
exports.Pokemon = Pokemon;
