import { Pokemon } from './pokemon';
export class Battle {
    public pokemonOne : Pokemon;
    public pokemonTwo : Pokemon;

    constructor(pokeOne : Pokemon, pokeTwo : Pokemon){
        let first_start = this.getPokemonStartFight(pokeOne, pokeTwo);
        if(first_start === 1){
            this.pokemonOne = pokeOne;
            this.pokemonTwo = pokeTwo
        }else{
            this.pokemonOne = pokeTwo;
            this.pokemonTwo = pokeOne;
        }
    }

    public getPokemonStartFight(poke_one:Pokemon, poke_two:Pokemon) : number {
        if(poke_one.speed < poke_two.speed){
            return 2;
        }else{
            return 1;
        }
    }

    public aAttaqueB(pokemonA:Pokemon , pokemonB:Pokemon) : Pokemon{
        console.log(pokemonA.name + " attaque " + pokemonB.name + " de " + pokemonA.damagePoint + " point(s)");
        pokemonB.receiveDamage(pokemonA.damagePoint)
        console.log("La vie de " + pokemonB + " est à " + pokemonB.lifePoint)
        return pokemonB;
    }

    public fight() : Pokemon{
        console.log("------------")
        console.log("DEBUT DU MATCH ")
        console.log("------------")
        
        console.log("Pokemon N°1 : " + this.pokemonOne.name + " " + this.pokemonOne.lifePoint)
        console.log("Pokemon N°2: " + this.pokemonTwo.name + " " + this.pokemonTwo.lifePoint)

        for(let i=0; this.pokemonOne.lifePoint>0 && this.pokemonTwo.lifePoint>0  ;i++){
            console.log("TOUR " + i)
            if(i%2===0){
                console.log(this.pokemonOne.name + " attaque " + this.pokemonTwo.name + " de " + this.pokemonOne.damagePoint + " point(s)");
                this.pokemonTwo.receiveDamage(this.pokemonOne.damagePoint)
                console.log("La vie de " + this.pokemonTwo.name + " est à " + this.pokemonTwo.lifePoint)
                // this.aAttaqueB(this.pokemonOne, this.pokemonTwo)
            }else{
                console.log(this.pokemonTwo.name + " attaque " + this.pokemonOne.name + " de " + this.pokemonTwo.damagePoint + " point(s)");
                this.pokemonOne.receiveDamage(this.pokemonTwo.damagePoint)
                console.log("La vie de " + this.pokemonOne.name + " est à " + this.pokemonTwo.lifePoint)
                // this.aAttaqueB( this.pokemonTwo, this.pokemonOne)
              
            }
        }

        console.log("------------")
        console.log("FIN DU MATCH ")
        console.log("------------")
        console.log("Pokemon N°1 : " + this.pokemonOne.name + " " + this.pokemonOne.lifePoint)
        console.log("Pokemon N°2: " + this.pokemonTwo.name + " " + this.pokemonTwo.lifePoint)
        console.log("------------")
        let pokemonWin = this.getWin(this.pokemonOne, this.pokemonTwo);
        console.log(pokemonWin.name + " gagne !")
        
        return pokemonWin;
    }

    public getWin(pokemonA:Pokemon, pokemonB:Pokemon) : Pokemon {
        if(pokemonA.lifePoint > pokemonB.lifePoint){
            return pokemonA;
        }
        else{ 
            return pokemonB;
        }
    }


}