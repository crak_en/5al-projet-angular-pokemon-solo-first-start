import {Battle} from "./battle";
import {Pokemon} from "./pokemon";



describe('First assign battle test', function () {
    
    let pikachu = new Pokemon("Pikachu", 50, 50, 10);
    let melophe = new Pokemon("Melophe", 50,30, 10);
    let dragonFeu =  new Pokemon("dragonFeu", 20, 20, 80);

    let first_battle = new Battle(pikachu, dragonFeu);

    it('Should give two' , function () {
        let val_starter = first_battle.getPokemonStartFight(dragonFeu, pikachu)
        expect(val_starter).toEqual(2);
    });

    test('Should give one',() => {
        let val_starter = first_battle.getPokemonStartFight(melophe, pikachu)
        expect(val_starter).toEqual(1);
    });

    
    
});


describe('First assign battle tesBat', function () {
    //Speed, lifePoint, damagePoint
    let pikachu = new Pokemon("Pikachu", 50, 50, 10);
    let melophe = new Pokemon("Melophe", 50,30, 10);
    let pikachu_fight_sim = new Pokemon("Pikachu", 50,0, 10);

    let first_battle = new Battle(pikachu, melophe);

    it('Pika wins' , function () {
        expect(first_battle.fight().lifePoint).toEqual(30);
    });
    
    
    it('Pika wins' , function () {
        expect(first_battle.getWin(pikachu_fight_sim, pikachu).name).toEqual("Pikachu");
    });

    it('Pika wins' , function () {
        expect(first_battle.getWin(pikachu, pikachu_fight_sim).name).toEqual("Pikachu");
    });
    
});



